#include <msp430.h>
/*
 * Testcode unit: Anti-bots sensor 
 * PEE20 lijnvolger 
 * Door Rens Bakker 
 * 0948583
 */

#define TRIGGER_PIN         BIT1  //pin 1.1
#define ECHO_PIN            BIT2  //pin 1.2
#define OUT_PIN             BIT3  //pin 1.3

int main()
{
    int echo_puls_duur;      // variabelen maken om de pulsduur en de afstand in op te slaan
    int afstand_cm;

    WDTCTL = WDTPW + WDTHOLD;      //Stop WDT
    if (CALBC1_1MHZ==0xFF)  //
       {                   //
           while(1);       //  DCO klok kalibreren
       }                   //
    BCSCTL1 = CALBC1_1MHZ;  //
    DCOCTL = CALDCO_1MHZ;   //
    P1DIR |= (TRIGGER_PIN | OUT_PIN); //pinnen poort 1 output
    P1DIR &= ~(ECHO_PIN);             //pinnen poort 1 input
    P1IN = BIT2;
    P1OUT = 0;

    TA1CTL |= (TASSEL_2 | ID_0 | MC_2); //Timer instellen

   while(1)
    {

        //Puls van 20 us op trigger pin
        P1OUT |= TRIGGER_PIN;
        __delay_cycles(20);
        P1OUT &= ~TRIGGER_PIN;

        while ((P1IN & ECHO_PIN) == 0)
        {
            //Wachten op het ontvangen van de echo-puls
        }
        TA1R = 0;                     //Als de echo puls ontvangen wordt , de teller van de timer resetten
        while ((P1IN & ECHO_PIN) > 0)
        {
            //Wachten op het einde van het ontvangen van de echo-puls
        }
        echo_puls_duur = TA1R;                   //variabele gelijk stellen aan de teller
        afstand_cm = ((echo_puls_duur * 0.017)); //de duur van de echo puls in us omzetten naar een afstand
        if ((afstand_cm <= 15))  //Als de afstand een bepaalde waarde heeft dan.....
        {
            P1OUT |= OUT_PIN; // LED aan
        }
        else P1OUT &= ~OUT_PIN;                 // Als de afstand een bepaalde waarde niet heeft dan..... 
    }
}
plak code hier